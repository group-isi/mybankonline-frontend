package com.isj.isi.Mybank.Dto;

import lombok.*;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class VirementDto {

    private  String senderaccountNumber;
    private String receiveraccountNumber;

    private Float montant;

    private String motifVirement;
}
