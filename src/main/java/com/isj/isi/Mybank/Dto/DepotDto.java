package com.isj.isi.Mybank.Dto;

import lombok.*;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class DepotDto {
    private String accountNumber;

    private Float montant;

    private String espece;

}
