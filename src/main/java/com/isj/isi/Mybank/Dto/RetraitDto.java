package com.isj.isi.Mybank.Dto;

import lombok.*;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@Getter
@Setter
public class RetraitDto {

    private String name;
    private String surname;
    private String accountNumber;
    //private String secretCode;
    private Float montant;
}
