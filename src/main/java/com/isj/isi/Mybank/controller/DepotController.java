package com.isj.isi.Mybank.controller;


import com.isj.isi.Mybank.Dto.DepotDto;
import com.isj.isi.Mybank.services.DepotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/register")
public class DepotController {

    protected final Logger log = LoggerFactory.getLogger(ClientController.class);

    private DepotDto depotDto;

    @Autowired
    private DepotService depotService;


    @GetMapping("/depot")
    public ModelAndView getDepotForm (){
        log.info("GET/register/depot called");
         String viewName= "/client/client-depot";
        Map<String,Object> model= new HashMap<String,Object>();
        model.put("depot", new DepotDto());
        return new ModelAndView(viewName, model);

    }

    /**
     *
     * @param depotDto
     * @return
     * @throws Exception
     */
    @PostMapping("/depot")
    public String getDepot (@Valid @ModelAttribute("depot") DepotDto depotDto, Model model) throws Exception {
        log.info("POST/register/depot called");

        this.depotDto= depotDto;

        depotService.createDepot(depotDto);
       model.addAttribute("depot",this.depotDto);

        return  "/client/Depot-Confirm";
    }

}
