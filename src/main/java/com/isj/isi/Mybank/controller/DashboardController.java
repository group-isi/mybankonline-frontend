package com.isj.isi.Mybank.controller;


import com.isj.isi.Mybank.entity.Client;
import com.isj.isi.Mybank.services.DemandService;
import com.isj.isi.Mybank.services.ManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping("/dashboard")
public class DashboardController {

    protected final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ManagerService managerService;

    @Autowired
    private DemandService demandService;


    // affichage du dashboard
    @GetMapping
    public ModelAndView getDashboard() {

        log.info("GET /dashboard called");
        String viewName = "dashboard/admin";
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("client", new Client());
        return new ModelAndView(viewName, model);


    }

    @GetMapping("/listaccounts")
    public ModelAndView getListAccountForm() throws Exception {

        log.info("GET /listAccount called");
        String viewName = "dashboard/ListAccount";
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("accounts", managerService.findAccounts());
        return new ModelAndView(viewName, model);


    }

    @GetMapping("/validateaccount")
    public String validateAccount(@RequestParam("code") String accountNumber) throws Exception {
        log.info("POST /dashboard/validateaccount called");

        managerService.validateAccount(accountNumber);

        return "/dashboard/AccountCreate";


    }

    @GetMapping("/deleteaccount")
    public String deleteAccount(@RequestParam("code") String accountNumber) throws Exception {
        log.info("POST /dashboard/deleteaccount called");

        managerService.deleteAccount(accountNumber);

        return "/dashboard/AccountDelete";


    }

    @GetMapping("/listdemands")
    public ModelAndView getListDemandsForm() throws Exception {

        log.info("GET /listdemands called");
        String viewName = "dashboard/ListDemand";
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("demands", demandService.findDemands());
        return new ModelAndView(viewName, model);


    }


}
