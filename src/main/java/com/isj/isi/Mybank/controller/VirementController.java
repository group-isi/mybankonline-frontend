package com.isj.isi.Mybank.controller;

import com.isj.isi.Mybank.Dto.VirementDto;
import com.isj.isi.Mybank.services.VirementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/register")
public class VirementController {

    protected final Logger log = LoggerFactory.getLogger(ClientController.class);

    private VirementDto virementDto;

    @Autowired
    private VirementService virementService;




    @GetMapping("/virement")
    public ModelAndView getVirementForm (){
        log.info("GET/register/virement called");
        String viewName= "/client/Client-Virement";
        Map<String,Object> model= new HashMap<String,Object>();
        model.put("virement", new VirementDto());
        return new ModelAndView(viewName, model);

    }

    /**
     *
     * @param virementDto
     * @return
     * @throws Exception
     */
    @PostMapping("/virement")
    public String getVirement (@Valid @ModelAttribute("virement") VirementDto virementDto, Model model) throws Exception {
        log.info("POST/register/depot called");

        this.virementDto= virementDto;

        virementService.createDepot(virementDto);
        model.addAttribute("virement",this.virementDto);

        return  "/client/Virement-Confirm";
    }

}
