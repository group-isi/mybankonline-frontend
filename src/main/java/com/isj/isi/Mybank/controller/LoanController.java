package com.isj.isi.Mybank.controller;

import com.isj.isi.Mybank.Dto.LoanDto;
import com.isj.isi.Mybank.services.PretService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/register")
public class LoanController {
    protected final Logger log = LoggerFactory.getLogger(ClientController.class);

    private LoanDto loanDto;

    @Autowired
    private PretService pretService;


    @GetMapping("/pret")
    public ModelAndView getPretForm (){
        log.info("GET/register/pret called");
        String viewName= "/client/client-Loan";
        Map<String,Object> model= new HashMap<String,Object>();
        model.put("loan", new LoanDto());
        return new ModelAndView(viewName, model);

    }

    @PostMapping("/pret")
    public String getDepot (@Valid @ModelAttribute("depot") LoanDto loanDto, Model model) throws Exception {
        log.info("POST/register/pret called");

        this.loanDto= loanDto;


        pretService.createLoan(loanDto);
        model.addAttribute("loan",this.loanDto);

        return  "/client/Loan-Confirm";
    }

}
