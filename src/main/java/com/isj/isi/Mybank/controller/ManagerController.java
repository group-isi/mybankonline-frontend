package com.isj.isi.Mybank.controller;

import com.isj.isi.Mybank.Dto.ClientDto;
import com.isj.isi.Mybank.Dto.ManagerDto;
import com.isj.isi.Mybank.entity.Manager;
import com.isj.isi.Mybank.services.ManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping("/register")
public class ManagerController {

    protected final Logger log = LoggerFactory.getLogger(ClientController.class);

    private ManagerDto managerDto;

    private ManagerService managerService = new ManagerService();


    // affichage du 1er formulaire
    @GetMapping("/manager")
    public ModelAndView getManagerRegistrationForm() {

        log.info("GET /register/manager called");

        String viewName = "manager/manager-registration-form";
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("manager", new ManagerDto());
        return new ModelAndView(viewName, model);
        /*String viewName= "manager/manager-registration-form";
        Map<String,Object> model= new HashMap<String,Object>();
        model.put("client",new ClientDto());
        return  new ModelAndView(viewName,model);
*/


    }

    // envoie du 1er formulaire au deuxième formulaire
    @PostMapping("/manager")
    public ModelAndView getManagerRegistration(@Valid @ModelAttribute("manager") ManagerDto managerDto) {
        log.info("POST/register/manager called");
        this.managerDto = managerDto;
        System.out.println(" dans le post du premier formulaire " + managerDto.toString());
        System.out.println("dans le 1er post le this.dto est " + this.managerDto.toString());
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("/register/manager/step-2");
        return new ModelAndView(redirectView);
    }


    //affichage du 2ème formulaire
    @GetMapping("/manager/step-2")
    public ModelAndView getCompleteRegistration() {
        log.info("GET /register/manager/step-2 called");
        final String viewName = "manager/manager-registration-step-2";
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("manager", this.managerDto);
        return new ModelAndView(viewName, model);
    }

    //envoie du 2ème formulaire au Dashboard
    @PostMapping("/manager/step-2")
    public ModelAndView getDashboard(@Valid @ModelAttribute("manager") ManagerDto managerDto) {
        log.info("POST/register/manager/step-2");

        this.managerDto = managerDto;

        System.out.println(" dans le post du 2e formulaire " + managerDto.toString());
        System.out.println("dans le 2e post le this.dto est " + this.managerDto.toString());
        Map<String, Object> model = new HashMap<>();

        try {
            final Manager manager = managerService.createManager(managerDto);
            RedirectView redirectView = new RedirectView();
            redirectView.setUrl("/dashboard");
            model.put("accounts", managerService.findAccounts());
            return new ModelAndView(redirectView, model);
        } catch (Exception e) {
            e.printStackTrace();
            return new ModelAndView("manager/manager-registration-step-2");
        }


    }
}



