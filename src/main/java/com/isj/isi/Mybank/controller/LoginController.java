package com.isj.isi.Mybank.controller;


import com.isj.isi.Mybank.Dto.AccountDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/login")
public class LoginController {

    protected final Logger log = LoggerFactory.getLogger(ClientController.class);


    private AccountDto accountDto;


    @GetMapping
    public ModelAndView getLoginForm(){

        log.info("GET /login");
        String viewName= "/client/login";
        Map<String,Object> model= new HashMap<String,Object>();
        model.put("account",new AccountDto() );
        return new ModelAndView(viewName, model);
    }

    @PostMapping("/listoperation")
    public String getlistoperationForm () throws Exception {
        log.info("POST/register/listoperation called");


        return  "/operationbancaire/operationlist";
    }

}
