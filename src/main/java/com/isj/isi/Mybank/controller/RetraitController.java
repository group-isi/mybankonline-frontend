package com.isj.isi.Mybank.controller;


import com.isj.isi.Mybank.Dto.RetraitDto;
import com.isj.isi.Mybank.services.RetraitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/register")
public class RetraitController {

    protected final Logger log = LoggerFactory.getLogger(ClientController.class);

    private RetraitDto retraitDto;

    @Autowired
    private RetraitService retraitService;

    @GetMapping("/retrait")
    public ModelAndView getRetraitForm (){
        log.info("GET/register/retrait called");
        String viewName= "/client/client-Retrait";
        Map<String,Object> model= new HashMap<String,Object>();
        model.put("retrait", new RetraitDto());
        return new ModelAndView(viewName, model);

    }


    @PostMapping("/retrait")
    public String getDepot (@Valid @ModelAttribute("depot") RetraitDto retraitDto, Model model) throws Exception {
        log.info("POST/register/depot called");

        this.retraitDto= retraitDto;


        retraitService.createRetrait(retraitDto);
        model.addAttribute("retrait",this.retraitDto);

        return  "/client/Retrait-Confirm";
    }
}
