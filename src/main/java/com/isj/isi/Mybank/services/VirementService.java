package com.isj.isi.Mybank.services;

import com.isj.isi.Mybank.Dto.VirementDto;
import com.isj.isi.Mybank.config.MyBankProperties;
import com.isj.isi.Mybank.entity.Virement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;


@Service
public class VirementService {

    public final Logger log = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private MyBankProperties properties;

    private RestTemplate restTemplate = new RestTemplate();


    public Virement createDepot(VirementDto virementDto) throws Exception {
        //log.info(JsonUtil.getJson(createDto));
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8000/api/virement/save");
        final URI url = builder.build().toUri();

        final HttpEntity<VirementDto> requestEntity = new HttpEntity<>(virementDto, requestHeaders);

        try {
            final ResponseEntity<Virement> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                    Virement.class);
            //final String jwtToken = response.getHeaders().get("Authorization").get(0);
            //session.setToken(jwtToken);
            // SecurityUtils.updateSecurityContextUser(jwtToken);
            //final Client client = response.getBody();
            //session.setCurrentCompany(client);
            return response.getBody();
        } catch (Exception e) {
            throw new Exception(e);
        }

    }
}
