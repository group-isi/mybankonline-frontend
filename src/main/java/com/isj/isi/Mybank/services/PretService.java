package com.isj.isi.Mybank.services;

import com.isj.isi.Mybank.Dto.LoanDto;
import com.isj.isi.Mybank.entity.Loan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
public class PretService {

    public final Logger log = LoggerFactory.getLogger(ClientService.class);

    private RestTemplate restTemplate = new RestTemplate();


    public Loan createLoan(LoanDto loanDto) throws Exception {
        //log.info(JsonUtil.getJson(createDto));
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8000/api/loan/save");
        final URI url = builder.build().toUri();

        final HttpEntity<LoanDto> requestEntity = new HttpEntity<>(loanDto, requestHeaders);

        try {
            final ResponseEntity<Loan> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                    Loan.class);
            //final String jwtToken = response.getHeaders().get("Authorization").get(0);
            //session.setToken(jwtToken);
            // SecurityUtils.updateSecurityContextUser(jwtToken);
            //final Client client = response.getBody();
            //session.setCurrentCompany(client);
            return response.getBody();
        } catch (Exception e) {
            throw new Exception(e);
        }


    }
}
