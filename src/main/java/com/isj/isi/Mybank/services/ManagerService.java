package com.isj.isi.Mybank.services;

import com.isj.isi.Mybank.Dto.ManagerDto;
import com.isj.isi.Mybank.config.MyBankProperties;
import com.isj.isi.Mybank.entity.Account;
import com.isj.isi.Mybank.entity.Manager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Service
public class ManagerService {
    public final Logger log = LoggerFactory.getLogger(ClientService.class);


    @Autowired
    private MyBankProperties properties;

    private RestTemplate restTemplate = new RestTemplate();

    public Manager createManager(ManagerDto managerDto) throws Exception {
        //log.info(JsonUtil.getJson(createManager()));
        System.out.println("hello");
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8000/api/managers/save");
        final URI url = builder.build().toUri();

        System.out.println(managerDto.toString());
        final HttpEntity<ManagerDto> requestEntity = new HttpEntity<>(managerDto, requestHeaders);

        try {
            System.out.println("dans manager service du front donne : " + managerDto);
            final ResponseEntity<Manager> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                    Manager.class);
            //final String jwtToken = response.getHeaders().get("Authorization").get(0);
            //session.setToken(jwtToken);
            // SecurityUtils.updateSecurityContextUser(jwtToken);
            //session.setCurrentCompany(client);
            return response.getBody();
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    //afficher la liste de demande de création d'un compte
    public List<Account> findAccounts() throws Exception {
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8000/api/managers/listAccount");
        final URI url = builder.build().toUri();

        final HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);
        try {
            final ResponseEntity<List<Account>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Account>>() {
            });
            return responseEntity.getBody();
        } catch (Exception e) {
            throw new Exception(e);
        }

    }

    public Account validateAccount(String accountNumber) throws Exception {
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        final UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl
                        ("http://localhost:8000/api/managers/validateAccount/" + accountNumber);
        final URI url = builder.build().toUri();

        final HttpEntity<String> requestEntity = new HttpEntity<>(accountNumber, requestHeaders);//requete http: createDto: corps de la requete, requestheader: entete
        try {
            final ResponseEntity<Account> response = restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
                    Account.class);//adresse, verbe: post ou Get,la requête,delivererDto
            return response.getBody();
        } catch (Exception e) {
            throw new Exception(e);
        }


    }

    public Account deleteAccount(String accountNumber) throws Exception {
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        final UriComponentsBuilder builder =
                UriComponentsBuilder.fromHttpUrl
                        ("http://localhost:8000/api/managers/deleteAccount/" + accountNumber);
        final URI url = builder.build().toUri();

        final HttpEntity<String> requestEntity = new HttpEntity<>(accountNumber, requestHeaders);//requete http: createDto: corps de la requete, requestheader: entete
        try {
            final ResponseEntity<Account> response = restTemplate.exchange(url, HttpMethod.PUT, requestEntity,
                    Account.class);//adresse, verbe: post ou Get,la requête,delivererDto
            return response.getBody();
        } catch (Exception e) {
            throw new Exception(e);
        }


    }
}


