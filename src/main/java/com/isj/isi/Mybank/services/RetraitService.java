package com.isj.isi.Mybank.services;

import com.isj.isi.Mybank.Dto.RetraitDto;
import com.isj.isi.Mybank.entity.Retrait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Service
public class RetraitService {
    public final Logger log = LoggerFactory.getLogger(ClientService.class);

    private RestTemplate restTemplate = new RestTemplate();


    public Retrait createRetrait(RetraitDto retraitDto) throws Exception {
        //log.info(JsonUtil.getJson(createDto));
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8000/api/retrait/save");
        final URI url = builder.build().toUri();

        final HttpEntity<RetraitDto> requestEntity = new HttpEntity<>(retraitDto, requestHeaders);

        try {
            final ResponseEntity<Retrait> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                    Retrait.class);
            //final String jwtToken = response.getHeaders().get("Authorization").get(0);
            //session.setToken(jwtToken);
            // SecurityUtils.updateSecurityContextUser(jwtToken);
            //final Client client = response.getBody();
            //session.setCurrentCompany(client);
            return response.getBody();
        } catch (Exception e) {
            throw new Exception(e);
        }


    }
}
