package com.isj.isi.Mybank.services;

import com.isj.isi.Mybank.Dto.DemandDto;
import com.isj.isi.Mybank.config.MyBankProperties;
import com.isj.isi.Mybank.entity.Demand_Suppression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;

@Service
public class DemandService {

    public final Logger log = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private MyBankProperties properties;

    private RestTemplate restTemplate= new RestTemplate();



    public Demand_Suppression createDemandSuppression (DemandDto demandDto) throws Exception{
        //log.info(JsonUtil.getJson(createDto));
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8000/api/demands/save");
        final URI url = builder.build().toUri();

        final HttpEntity<DemandDto> requestEntity = new HttpEntity<>(demandDto, requestHeaders);

        try {
            final ResponseEntity<Demand_Suppression> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                    Demand_Suppression.class);
            //final String jwtToken = response.getHeaders().get("Authorization").get(0);
            //session.setToken(jwtToken);
            // SecurityUtils.updateSecurityContextUser(jwtToken);
            //final Client client = response.getBody();
            //session.setCurrentCompany(client);
            return response.getBody();
        }
        catch (Exception e) {
            throw new Exception(e);
        }

    }



    public List<Demand_Suppression> findDemands() throws Exception{
        final HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);

        final UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://localhost:8000/api/demands/listDemands");
        final URI url = builder.build().toUri();

        final  HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);
        try {
            final ResponseEntity<List<Demand_Suppression>> responseEntity= restTemplate.exchange(url, HttpMethod.GET, requestEntity, new ParameterizedTypeReference<List<Demand_Suppression>>() {
            });
            return  responseEntity.getBody();
        }
        catch (Exception e){
            throw new  Exception(e);
        }

    }


}
