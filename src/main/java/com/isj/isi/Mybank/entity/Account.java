package com.isj.isi.Mybank.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;



@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Data
public class Account extends Abstract_Mybank_Entity{

    @NotEmpty

    private String accountNumber;
    @NotEmpty
    private Long balanceAccount;

    @NotEmpty
    private String status;


    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "manager")
    private Manager manager;


    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "client")
    private Client client;



}
