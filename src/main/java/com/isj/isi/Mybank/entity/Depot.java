package com.isj.isi.Mybank.entity;

import lombok.*;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Data
@EqualsAndHashCode
@NoArgsConstructor
@Getter
@Setter
public class Depot extends Abstract_Mybank_Entity {

    private String accountNumber;

    private Float montant;

    private String espece;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name ="account")
    private Account account;


}
