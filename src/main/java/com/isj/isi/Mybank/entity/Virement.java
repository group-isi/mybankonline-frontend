package com.isj.isi.Mybank.entity;

import lombok.*;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
public class Virement extends Abstract_Mybank_Entity {

    private  String senderaccountNumber;
    private String receiveraccountNumber;
    private Float montant;
    private String motifVirement;

    @ManyToOne
    @JoinColumn(referencedColumnName = "id", name = "account")
    private Account account;
}
