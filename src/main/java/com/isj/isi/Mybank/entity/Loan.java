package com.isj.isi.Mybank.entity;

import lombok.*;

import javax.persistence.Entity;


@Data
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
public class Loan extends Abstract_Mybank_Entity {

    private String name;
    private String surname;
    private String sexe;
    private Float Salaire;
    private String accountNumber;
    private Float amountRequest;
    private String refundMethod;

}
