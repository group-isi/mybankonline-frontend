package com.isj.isi.Mybank.entity;


import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@EqualsAndHashCode
public class Manager extends User {
    @NotEmpty
private int cni;

    @NotEmpty
    private String password;

    private String birthday;
}

